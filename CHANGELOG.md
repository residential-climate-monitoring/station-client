# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres
to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [New]



## [0.5.0]

### Added

* Added support for the heater on the SHT31-D sensor.
* "-" in sensor settings are now replaced with "_" to be more permissive. 
* Added support for configuring gain and integration time for TSL2591 via sensor settings.

### Deprecated

* The settings `tsl2591-gain` and `tsl2591-integration-time` are deprecated in favour of sensor settings from the server. Settings from
  the server will take precedence and the local settings will only be used as fallback. They will be removed in the next major release.

### Fixed

* Issue with duplicate process detection not working correctly on Linux. 

## [0.4.1]

### Fixed

* It will no longer raise an exception when no sensors are configured in the .env file. This should be optional since it only serves as a 
  fallback and the primary source is the server.

### Changed

* Added a console script entry point to setup.py to enable `rcm-station-client` from the bin directory of the virtualenv. This eliminates 
  the need to reference the `venv/lib/python3.x/site-packages` which is version specific. 

## [0.4.0]

### Added

* Added `TmpHumSensor` base parent class for sensors that measure temperature and (relative) humidity. Implemented by SHT31-D and BME680.
* The `eCO2` and `TVOC` metrics of the SGP30 sensor are now automatically compensated for the temperature and humidity when a TmpHumSensor
  is detected.
* The station-client now detects if it is already running and stops if it does. 

### Changed

* The baseline initialization for the SGP30 sensor starts automatically when no baseline is found or the baseline is older than 7 days.

### Removed

* The `sgp30-init-baseline` setting is removed in favour of automatic baseline initialization. To force baseline initialization, simply 
  remove/rename the baseline file.

## [0.3.0] - unlisted

### Added

* Retrieving list of sensors for station from API.
* Retrieving sensor settings from API.
* Enabled support for secondary I2C address for the SHT31-D sensor.
* Added `cpu-percent` metric for SYS sensor.
  *Added `disable-i2c` option for easier testing and to be able to run it on environments without I2C, like development machine.
* Configuration options:
    * `disable-i2c` to disable I2C so it's easier to run on a development machine.
    * `log-include-console-handler` to print all logging also to the console, for ease of development.
    * `api-base-url` the base url that points to the API.

### Changed

* the `sensors` configuration option is no longer leading. The response from the API is. This setting is only used as fallback in case of an
  exception from the api.

### Removed

* Configuration option `measurements-report-endpoint`, replaced by `api-base-url`

## [0.2.0]

### Added

Added support for SYS sensor - which just reads system metrics using psutil.

## [0.1.0]

### Added

Added support for SGP30 sensor.

[0.5.0]: https://gitlab.com/residential-climate-monitoring/station-client/-/releases/v0.5.0
[0.4.1]: https://gitlab.com/residential-climate-monitoring/station-client/-/releases/v0.4.1
[0.4.0]: https://gitlab.com/residential-climate-monitoring/station-client/-/releases/v0.4.0
[0.2.0]: https://gitlab.com/residential-climate-monitoring/station-client/-/releases/v0.2.0
[0.1.0]: https://gitlab.com/residential-climate-monitoring/station-client/-/releases/v0.1.0